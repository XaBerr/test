# test

```plantuml
@startuml
!define DARK
!includeurl https://raw.githubusercontent.com/XaBerr/plantUMLstyle/master/style.plantuml
hide empty description

Src : 
Sensors -up-> Src
Controllers -up-> Src
Actuators -up-> Src
Logs -up-> Src
Setups -up-> Src

Camera -up-> Sensors
PSD -up-> Sensors

Direct -up-> Controllers
KPA -up-> Controllers
PID -up-> Controllers

Mirror -up-> Actuators

LogFile -up-> Logs
LogVideo -up-> Logs

Matera_10_10_2018 -up-> Setups
Matera_10_12_2018 -up-> Setups
Qucosone_07_10_2019 -up-> Setups
@enduml
```